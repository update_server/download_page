var slideIndex = 0;

showSlides();
//slider method
function showSlides() {
	var i;
	var slides = document.getElementsByClassName("mySlides");
	var dots = document.getElementsByClassName("dot");
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	slideIndex++;
	if (slideIndex > slides.length) {slideIndex = 1}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" active", "");
	}
	slides[slideIndex-1].style.display = "block";
	dots[slideIndex-1].className += " active";
	setTimeout(showSlides, 2000); // Change image every 2 seconds
}

//slide up and slide down toggle
function showInstruction(e){
	//document.querySelector('.footer').style.display="block";
	slide = document.querySelector('.photos');
	slide.style.height="22.8rem";
	slide.classList.remove('slide-up');
	slide.classList.add('slide-down');
	setScrollHeight();
	//document.querySelector(".footer").style.display="block";
}

function setScrollHeight(){
	var header = document.querySelector(".header").offsetHeight;
	var slide = document.querySelector(".slide").offsetHeight;
	var offsetHeight = header + slide;
	setTimeout(backToTopAnimation(offsetHeight),100);
}

// initially the opacity animation isn't hidden ,this timer is to resolve this problem.
setTimeout(function(){
	document.querySelector(".header-ios-android").style.opacity=1;
},1000)

//animation photo on the top
window.onload=function(){
 var logo = document.querySelector(".top-logo ");
	logo.classList.remove('move-0');
	logo.classList.add("move-05");
	judegeWidthAndHeight();
}

function androidLink(event){
	window.location.href = "http://xxx68.enthtx.com/bojing.apk";
}

function iosLink(event){
	var i = Math.round(Math.random() * 10);
    if (i % 2 >= 0) {
        location.href = "itms-services://?action=download-manifest&url=https://down.jczx16.com/bojingMA.plist";
    }
}
function backToTop(e){ //点击回到顶部动画
	var scrollToptimer = setInterval(function () {
		    var top = document.body.scrollTop || document.documentElement.scrollTop;
		    var speed = top / 4;
		    if (document.body.scrollTop!=0) {
		        document.body.scrollTop -= speed;
		    }else {
		        document.documentElement.scrollTop -= speed;
		    }
		    if (top == 0) {
		        clearInterval(scrollToptimer);
		    }
		}, 10); 
}

function backToTopAnimation(offsetHeight){//点击查看说明时的滚动条动画
	var timer = setInterval(function () {
		    var top = document.body.scrollTop || document.documentElement.scrollTop;
		    if (top!=offsetHeight) {
		        top = top+15;
		        window.scrollTo(0,top);
		    }
		    if (top >= offsetHeight) {
		        clearInterval(timer);
		    }
		}, 1); 
}

function bodyScroll(e){//滚动滚动条，出现回到顶部
	 var top = document.body.scrollTop || document.documentElement.scrollTop;
	 var backToTop = document.querySelector(".back-to-top");
	 if(top===0){
	 	backToTop.style.display="none";
	 }else{
	 	backToTop.style.display="block";
	 }
}

//判断logo图片宽高
function judegeWidthAndHeight(){
	var picElement = document.querySelector(".logo img");
	if(picElement.offsetHeight > picElement.offsetWidth){//图片宽大于高
		var divEle = document.querySelector(".logo");
		divEle.style.height="1.4rem";
		divEle.style.top="0rem";
	}
}